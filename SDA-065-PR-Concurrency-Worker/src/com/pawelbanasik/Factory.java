package com.pawelbanasik;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class Factory implements Observer{
//	private List<FactoryWorker> workers;
	
	public void addTask(long timeout, String name){
		FactoryWorker worker = new FactoryWorker(timeout, name);
		worker.addObserver(this);
		
		new Thread(worker).start();
//		new Thread(new Runnable(){
//			@Override
//			public void run() {
//				// TODO Auto-generated method stub
//				
//			}
//		}).start();
	}
	
	@Override
	public void update(Observable o, Object arg) {
		if( o instanceof FactoryWorker){
			FactoryWorker worker = (FactoryWorker)o ;
			
			System.out.println(worker.getName() + " skończył pracę i zwrócił " + arg + " produktów.");
		}
	}
}
