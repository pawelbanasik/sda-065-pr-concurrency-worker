package com.pawelbanasik;

import java.util.Observable;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class FactoryWorker extends Observable implements Runnable{
	
	private long workTime;
	private String name;
	
	public FactoryWorker(long workTime, String name) {
		super();
		this.workTime = workTime;
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public void run() {
		System.out.println("Wątek "+name+" zaczyna pracę.");
		try{
			Thread.sleep(workTime);
		}catch (InterruptedException e) {
			System.err.println("Error, wątek przerwany");
		}
		
		setChanged();
		notifyObservers(new Random().nextInt(10));
	}
}
