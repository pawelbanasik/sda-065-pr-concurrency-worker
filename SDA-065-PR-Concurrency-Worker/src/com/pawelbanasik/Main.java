package com.pawelbanasik;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Factory f = new Factory();
		
		Scanner sc = new Scanner(System.in);
		while(sc.hasNextLine()){
			String line = sc.nextLine();
			String[] splits = line.split(" ");
			
			f.addTask(Long.parseLong(splits[0]), splits[1]);
		}
	}
}
